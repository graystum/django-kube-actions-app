## Initialize and format terraform

```
cd terraform/eks
terraform init
terraform fmt
terraform validate
```

## Build the eks cluster

```
terraform plan 
terraform apply -auto-approve
```

## Refresh the kubectl cluster config file && add gitlab k8s-agent
```
aws eks update-kubeconfig --region eu-north-1 --name django-k8s
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set config.token=XXXXXXXXXXXXXXXXXXXXXXXX \
    --set config.kasAddress=wss://kas.gitlab.com
```

## Deploy the ALB controller using kubectl 

```
cd ../../k8s
Install cert-manager so that you can inject the certificate configuration into the webhooks.
kubectl apply --validate=false -f addons/cert-manager.yaml
# Install the AWS Load Balancer Controller CRDs - Ingress Class Params and Target Group Bindings
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"
# deploy the ingress controller
kubectl apply -f ingress/ingress-controller.yaml
```

## Deploy cluster secrets

```
Install cert-manager so that you can inject the certificate configuration into the webhooks.
kubectl apply -f addons/registry-credentials.yaml
# crete django environement secrets on cluster
kubectl create secret generic django-k8s-web-env --from-env-file=web/.env.prod 
```

## Deploy the ingress

```
# deploy the service and you deployment.apps
kubectl apply -f apps/django-k8s-web.yaml
kubectl apply -f apps/django-k8s-web-service.yaml
# deploy the ingress 
kubectl apply -f ingress/ingress.yaml
```

## Now the CI is set, you can make changes to the django application and push to gitlab which will auto deploy the chnages on your pods
