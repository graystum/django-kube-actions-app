##########################
# PROVIDER CONFIGURATION
##########################

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Automation  = "Terraform"
      Subsystem   = "django-k8s"
      Environment = "Dev"
    }
  }
}

terraform {
  required_version = ">=1.1.6"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.6"
    }
  }
}

