variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "dev"
}

variable "region" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "eu-north-1"
}