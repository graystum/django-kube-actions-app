##########################
# Defining locals
##########################

locals {
  name   = var.name
  cluster_name = "django-k8s"
  region = var.region
  #   tags = {
  #     Name        = "production"
  #   }
}


################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "git::git@gitlab.com:graystum/TF-modules.git//modules/vpc?ref=v1.0.0"

  name = local.name
  cidr = "17.0.0.0/16" # 10.0.0.0/8 is reserved for EC2-Classic

  azs             = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets = ["17.0.1.0/24", "17.0.2.0/24", "17.0.3.0/24"]
  public_subnets  = ["17.0.11.0/24", "17.0.12.0/24", "17.0.13.0/24"]

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default" }

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = 1
  }
}

################################################################################
# VPC Endpoints Module
################################################################################

module "vpc_endpoints" {
  source = "git::git@gitlab.com:graystum/TF-modules.git//modules/vpc/vpc-endpoints?ref=v1.0.0"

  vpc_id             = module.vpc.vpc_id
  security_group_ids = [data.aws_security_group.default.id]

  endpoints = {
    s3 = {
      service      = "s3"
      service_type = "Gateway"
      tags         = { Name = "s3-vpc-endpoint" }
    },
    dynamodb = {
      service         = "dynamodb"
      service_type    = "Gateway"
      route_table_ids = flatten([module.vpc.private_route_table_ids])
      policy          = data.aws_iam_policy_document.dynamodb_endpoint_policy.json
      tags            = { Name = "dynamodb-vpc-endpoint" }
    },
  }
}

################################################################################
# Route Associations
################################################################################

resource "aws_vpc_endpoint_route_table_association" "s3_endpoint" {
  vpc_endpoint_id = module.vpc_endpoints.endpoints.s3.id
  route_table_id  = module.vpc.private_route_table_ids
}

resource "aws_vpc_endpoint_route_table_association" "dynamodb_endpoint" {
  vpc_endpoint_id = module.vpc_endpoints.endpoints.dynamodb.id
  route_table_id  = module.vpc.private_route_table_ids
}

################################################################################
# Supporting Resources
################################################################################

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

data "aws_iam_policy_document" "dynamodb_endpoint_policy" {
  statement {
    effect    = "Deny"
    actions   = ["dynamodb:*"]
    resources = ["*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "StringNotEquals"
      variable = "aws:sourceVpce"

      values = [module.vpc.vpc_id]
    }
  }
}

module "web-public" {
  source = "git::git@gitlab.com:graystum/TF-modules.git//modules/security-group?ref=v1.0.0"

  create_sg   = true
  name        = "${local.name}-public"
  description = "Allow HTTP/S,ICMP, inbound traffic from world"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      description = "TLS from world"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      description = "HHTP from world"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      description = "ICMP from world"
      from_port   = -1
      to_port     = -1
      protocol    = "icmp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

module "http-private" {
  source = "git::git@gitlab.com:graystum/TF-modules.git//modules/security-group?ref=v1.0.0"

  create_sg   = true
  name        = "${local.name}-alb-private"
  description = "Allow HTTP inbound traffic from web public security group alone"
  vpc_id      = module.vpc.vpc_id

  ingress_with_source_security_group_id = [
    {
      description              = "http from web-public secutiy group only"
      from_port                = 80
      to_port                  = 80
      protocol                 = "tcp"
      source_security_group_id = module.web-public.security_group_id
    }
  ]
}
