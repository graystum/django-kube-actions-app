provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Automation  = "Terraform"
      Subsystem   = "django-k8s"
      Environment = "Dev"
    }
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    # load_config_file       = false
    config_path = "~/.kube/config"
  }
}

terraform {
  required_version = ">=1.1.6"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.72"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.0"
    }
  }
}
