##########################
# TERRAFORM CONFIGURATION
##########################

terraform {
  backend "s3" {
    bucket         = "graystum-terraform"
    key            = "django-k8s/cluster"
    region         = "us-east-1"
    dynamodb_table = "terraform_state_lock"
    encrypt        = true
  }
}

##########################
# Locals
##########################

locals {
  name            = "django-k8s"
  cluster_version = "1.21"
  vpc_id          = data.aws_vpc.vpc.id
  private_subnets = [data.aws_subnet.priv-3.id, data.aws_subnet.priv-2.id, data.aws_subnet.priv-1.id]
  eks_node_role   = data.aws_iam_role.node.arn
}

##########################
# Data Sources
##########################

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["dev"]
  }
}

data "aws_iam_role" "node" {
  name = "g-eks-node-role"
}

data "aws_subnet" "priv-1" {
  filter {
    name   = "tag:Name"
    values = ["dev-private-eu-north-1a"]
  }
}
data "aws_subnet" "priv-2" {
  filter {
    name   = "tag:Name"
    values = ["dev-private-eu-north-1b"]
  }
}
data "aws_subnet" "priv-3" {
  filter {
    name   = "tag:Name"
    values = ["dev-private-eu-north-1c"]
  }
}

data "aws_key_pair" "key" {
  key_name = "g_stockholm"
}

data "aws_eks_cluster" "cluster" {
  depends_on = [module.eks]
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  depends_on = [module.eks]
  name = module.eks.cluster_id
}

################################################################################
# EKS Module
################################################################################

module "eks" {
  source = "git::git@gitlab.com:graystum/TF-modules.git//modules/eks?ref=v1.0.5"

  cluster_name                    = local.name
  cluster_version                 = local.cluster_version
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  # IPV4
  cluster_ip_family = "ipv4"

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts        = "OVERWRITE"
      service_account_role_arn = module.vpc_cni_irsa.iam_role_arn
    }
  }

  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.eks.arn
    resources        = ["secrets"]
  }]

  vpc_id     = local.vpc_id
  subnet_ids = local.private_subnets

  # Extend cluster security group rules
  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  # Extend node-to-node security group rules
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  eks_managed_node_group_defaults = {
    min_size                   = "1"
    max_size                   = "4"
    desired_size               = "2"
    ami_type                   = "AL2_x86_64"
    disk_size                  = 40
    instance_types             = ["t3.medium"]
    iam_role_attach_cni_policy = true
    iam_role_arn               = local.eks_node_role
    key_name                   = data.aws_key_pair.key.key_name
    tags = {
      "kubernetes.io/cluster/${local.name}" = "owned"
    }
  }

  eks_managed_node_groups = {
    # Default node group - as provided by AWS EKS
    default_node_group = {
      name                   = "django-k8s-nodes"
      create_launch_template = false
      launch_template_name   = ""
      capacity_type          = "SPOT"
      update_config = {
        max_unavailable_percentage = 50
      }
      iam_role_additional_policies = [aws_iam_policy.worker_policy.arn]

      # Remote access cannot be specified with a launch template
      remote_access = {
        ec2_ssh_key = data.aws_key_pair.key.key_name
      }
    }
  }
}


################################################################################
# ALB Ingress Controller
################################################################################

resource "helm_release" "ingress" {
  name       = "ingress"
  chart      = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  version    = "1.4.1"
  wait       = false
  namespace = "kube-system"

  set {
    name  = "autoDiscoverAwsRegion"
    value = "true"
  }
  set {
    name  = "autoDiscoverAwsVpcID"
    value = "true"
  }
  set {
    name  = "clusterName"
    value = local.name
  }
}

################################################################################
# Supporting Resources
################################################################################

resource "aws_kms_key" "eks" {
  description             = "EKS Secret Encryption Key"
  deletion_window_in_days = 7
  enable_key_rotation     = true

}

module "vpc_cni_irsa" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "~> 4.12"

  role_name_prefix      = "VPC-CNI-IRSA"
  attach_vpc_cni_policy = true
  vpc_cni_enable_ipv6   = true

  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-node"]
    }
  }
}

resource "aws_iam_policy" "worker_policy" {
  name        = "worker-policy"
  description = "Worker policy for the ALB Ingress"

  policy = file("iam-policy.json")
}

